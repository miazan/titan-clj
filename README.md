# titan-clj

A small wrapper around [titan](https://github.com/thinkaurelius/titan) java api

## Usage

### Schema related functions
All schema creation functions take ```Management System``` as first argument.

A vertex label is created as

```
(make-vertex-label! mgmt :user :static? true :partition? false)
```

An edge label is created as

```
(make-edge-label! mgmt :works-for :multiplicity :one-to-many
 :signature [since] :directed? true)
```

A property key is created as

```
(make-property-key! mgmt :title String :cardinality :list
 :signature :uid)
```

### TODO
Tests and documentation for index building functions

## License

Copyright © 2015 Miazan Infotech

Distributed under the Eclipse Public License either version 1.0 or (at your option) any later version.
