(defproject miazan/titan-clj "0.1.2-SNAPSHOT"
  :description "Small wrapper around titan java api"
  :url "http://bitbucket.org/miazan/titan-clj"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :warn-on-reflection true
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [com.thinkaurelius.titan/titan-core "0.5.4"]
                 [clojurewerkz/ogre "2.5.0.0"]]
  :profiles
  {:dev {:dependencies
         [[com.thinkaurelius.titan/titan-berkeleyje "0.5.4"]]}})
