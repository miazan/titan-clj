(ns titan-clj.schema-test
  (:import [com.tinkerpop.blueprints Edge Vertex])
  (:require [titan-clj.schema :refer :all]
            [titan-clj.core :refer :all]
            [clojure.test :refer :all]))

(use-fixtures :each
  (fn [f]
    (let [g (create-graph! {"storage.backend" "inmemory"
                            "schema.default" "none"})
          m (.getManagementSystem g)]
      (doto m
        (make-property-key! :title String)
        (make-property-key! :from-date java.util.Date)
        (make-property-key! :email String :cardinality :set)
        (make-edge-label! :works-for :multiplicity :multi
                          :signature [:from-date])
        (make-vertex-label! :user :static? true :partition? false)
        (build-composite-index! "user-by-email" Vertex
                                :index-only :user
                                :keys [:email])
        (build-edge-index! "works-for" :works-for
                           :direction :both
                           :order :desc
                           :keys [:from-date])
        (.commit))
      (binding [*g* g]
        (f)))))

(deftest property-key-is-created
  (let [m (.getManagementSystem *g*)]
    (is (.containsPropertyKey m "title"))))

(deftest edge-label-is-created
  (let [m (.getManagementSystem *g*)]
    (is (.containsEdgeLabel m "works-for"))))

(deftest vertex-label-is-created
  (let [m (.getManagementSystem *g*)]
    (is (.containsVertexLabel m "user"))))
