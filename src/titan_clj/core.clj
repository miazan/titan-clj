(ns titan-clj.core
  (:import [com.thinkaurelius.titan.core RelationType Multiplicity
            TitanTransaction Cardinality Order PropertyKey EdgeLabel
            TitanFactory TitanFactory$Builder TitanGraph TitanVertex
            TitanProperty TitanEdge]
           [com.thinkaurelius.titan.graphdb.blueprints TitanBlueprintsGraph]
           [com.tinkerpop.blueprints Edge Vertex Direction]
           [java.util Date])
  (:require [clojurewerkz.ogre.tinkergraph :as g]
            [clojurewerkz.ogre.pipe :as ogre-pipe]
            [clojurewerkz.ogre.core :as q]))

(def ^:dynamic ^TitanGraph *g*)

(defn commit [^TitanBlueprintsGraph tx] (.commit tx))

(defn rollback [^TitanBlueprintsGraph tx] (.rollback tx))

(defmacro transact!
  ([body] `(transact! *g* ~body))
  ([g body]
   `(try
      (let [ret# ~body]
        (.commit *g*)
        ret#)
      (catch Exception ex#
        (.rollback *g*)
        (throw ex#)))))

(defmacro do-transact!
  [& body]
  `(transact! *g* ~@body))

(defn label
  "Get vertix label"
  [^TitanVertex v]
  (keyword (.getLabel v)))

(defn vertex?
  "Whether v is a vertex"
  [v]
  (instance? Vertex v))

(defn edge? [e]
  "Whether e is an edge"
  (instance? Edge e))

(defn prop
  "Get property for a given key"
  [^Vertex v property-key]
  (.getProperty v (name property-key)))

(defn id [^Vertex v]
  (if (vertex? v)
    (.getId v)
    (:id v)))

(defn props
  "Get properties of a given vertex. If key is given only
   property keys with the given key value are returned"
  ([^TitanVertex v key]
   (if-let [properties (.getProperties v (name key))]
     (map (fn [^TitanProperty p] (.getValue p))
          (iterator-seq (.iterator properties)))
     (empty)))
  ([^TitanVertex v]
   {:pre [(some? v)]}
   (into {:id (id v)}
         (for [^TitanProperty p (.getProperties v)]
           [(keyword (.getName (.getPropertyKey p))) (.getValue p)]))))

(defn vertex
  ([id] (vertex *g* id))
  ([^TitanGraph g ^Vertex v]
   (let [vid (if (instance? Vertex v) (id v) v)]
     (.getVertex g vid))))

(defn set-properties!
  "Set properties of a vertex"
  [^Vertex el props]
  (doseq [[k v] props]
    (let [v (if (keyword? v) (name v) v)]
      (.setProperty el (name k) v))))

(defn add-vertex!
  "Add a vertex"
  ([vertex-name props]
   (add-vertex! *g* vertex-name props))
  ([^TitanTransaction tx vertex-name props]
   (let [props (assoc props :created-on (Date.) :last-modified-on (Date.))
         vertex (.addVertexWithLabel tx (name vertex-name))]
     (set-properties! vertex props)
     vertex)))

(defn add-edge!
  "Add an edge"
  [^Vertex src edge-name ^Vertex dest & [props]]
  {:pre [(some? src) (some? dest)]}
  (let [edge (.addEdge src (name edge-name) dest)]
    (set-properties! edge props)
    edge))

(defn set-property!
  "Set a property value for a vertex"
  [^Vertex v key value]
  (doto v
    (.setProperty (name key) value)
    (.setProperty "last-modified-on" (Date.))))

(defn remove-edge!
  "Remove an edge"
  [^TitanEdge e]
  (.remove e))

(defn remove-vertex!
  "Remove a vertex"
  [^TitanVertex v]
  (.remove v))

(defn add-property!
  "Add property value for a list or set property type"
  [^TitanVertex v property-key & values]
  (doseq [value values]
    (.addProperty v (name property-key) value)))

(defn remove-property!
  "Remove a property"
  [^TitanVertex v property-key]
  (if (prop v (name property-key))
    (.removeProperty v (.getRelationType *g* (name property-key)))))

(defn all-vertices
  "Get all vertices"
  ([] (all-vertices *g*))
  ([^TitanGraph g]
   (.getVertices g)))

(defn vertices-by-label
  "Get all vertices with a given label"
  ([label] (vertices-by-label *g* label))
  ([^TitanGraph g label]
   (.. g
       (query)
       (has "label" (name label))
       (vertices))))


(defmethod ogre-pipe/convert-to-map clojure.lang.PersistentArrayMap
  [m]
  (into {} (for [[k v] m] [(keyword k) v])))


(defn map-with-id
  "Transform into a map but vertex id also set"
  [t & keys]
  (q/transform t (fn [^Vertex v]
                   (assoc (props v) :id (.getId v)))))

