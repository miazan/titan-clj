(ns titan-clj.schema
  (:import [com.thinkaurelius.titan.core RelationType Multiplicity
            Cardinality Order PropertyKey EdgeLabel TitanGraph
            TitanFactory TitanFactory$Builder]
           [com.thinkaurelius.titan.graphdb.database.management
            ManagementSystem]
           [com.thinkaurelius.titan.core.schema PropertyKeyMaker
            EdgeLabelMaker VertexLabelMaker TitanManagement
            TitanManagement$IndexBuilder Parameter TitanSchemaElement
            ConsistencyModifier]
           [com.tinkerpop.blueprints Edge Vertex Direction]))

(defn- ^Multiplicity to-multiplicity [m]
  (case m
    :many-to-one Multiplicity/MANY2ONE
    :multi Multiplicity/MULTI
    :one-to-many Multiplicity/ONE2MANY
    :one-to-one  Multiplicity/ONE2ONE
    :simple Multiplicity/SIMPLE
    (throw (ex-info "invalid multiplicity" {:multiplicity m}))))

(defn- to-cardinality [c]
  (case c
    :single Cardinality/SINGLE
    :list Cardinality/LIST
    :set Cardinality/SET
    (throw (ex-info "invalid cardinality" {:cardinality c}))))

(defn- to-direction
  [direction]
  (case direction
    :out Direction/OUT
    :in  Direction/IN
    :both Direction/BOTH
    (throw (ex-info "invalid direction" {:direction direction}))))

(defn- to-order
  [order]
  (case order
    :asc Order/ASC
    :desc Order/DESC
    (throw (ex-info "invalid order" {:order order}))))

(defn ^RelationType relation-type
  "Get relation type"
  [^ManagementSystem mgmt key]
  {:post [(some? %)]}
  (.getRelationType mgmt (name key)))

(defn vertex-label
  [^ManagementSystem mgmt key]
  (.getVertexLabel mgmt (name key)))

(defn edge-label
  [^ManagementSystem mgmt key]
  (.getEdgeLabel mgmt (name key)))

(defn ^EdgeLabelMaker set-signature
  [^EdgeLabelMaker maker ^ManagementSystem mgmt s]
  (let [#^"[Lcom.thinkaurelius.titan.core.RelationType;"
        s (->> (if (coll? s) s [s])
               (map #(if (keyword? %) (relation-type mgmt %) %))
               (into-array))]        
    (.signature maker s)))

(defn make-edge-label!
  "Create an edge-label. Label can be configured with optional parameters.
   Multiplicity can be set to :multi, :single, :one-to-many, :many-to-one
   and one-to-one. directed? should be set to true if edge is directed 
   otherwise set to false. signature can be set to a signature property key"
  [^ManagementSystem mgmt
   edge-label
   & {:keys [multiplicity signature directed?]
      :or {directed? true}}]
  (let [^EdgeLabelMaker maker
        (cond-> (.makeEdgeLabel mgmt (name edge-label))
          directed? (.directed)
          (not directed?) (.unidirected)
          (some? multiplicity) (.multiplicity (to-multiplicity multiplicity))
          (some? signature) (set-signature mgmt signature))]
    (.make maker)))

(defn make-vertex-label!
  "Create a vertex label. Label can be set to static or partitioned using
   options static? and partition? respectivily"
  [^TitanManagement mgmt
   vertex-label
   & {:keys [static? partition?] :or {static? false partition? false}}]
  (let [^VertexLabelMaker maker (.makeVertexLabel mgmt (name vertex-label))]
    (.make
     (cond-> maker
       static?     (.setStatic)
       partition?  (.partition)))))

(defn make-property-key!
  "Create a property key for a given property name and type. Cardinality
   can be set using opts. The cardinality can be set to :list, :single or :set.
   Signature can be set to a property key"
  [^TitanManagement mgmt
   property-name
   ^Class data-type
   & {:keys [cardinality signature] :as opts}]
  (let [^PropertyKeyMaker maker
        (cond-> (.makePropertyKey mgmt (name property-name))
          (some? cardinality) (.cardinality (to-cardinality cardinality))
          (some? data-type)   (.dataType data-type)
          (some? signature)   (set-signature mgmt signature))]
    (.make maker)))

(defn- ^TitanManagement$IndexBuilder add-index-keys
  [^TitanManagement$IndexBuilder builder ^ManagementSystem mgmt keys]
  (loop [keys (if (coll? keys) keys [keys]) builder builder]
    (if-let [key (first keys)]
      (recur (rest keys)
             (if (coll? key)
               (.addKey builder (first (relation-type mgmt key))
                        (into-array Parameter (second key)))
               (.addKey builder (relation-type mgmt key))))
      builder)))

(defn ^TitanManagement$IndexBuilder index-builder
  [^TitanManagement mgmt
   ^String name
   ^Class el-type
   {:keys [keys unique? index-only]}]
  (let [^TitanManagement$IndexBuilder builder (.buildIndex mgmt name el-type)]
    (cond-> builder
      (some? key) (add-index-keys mgmt keys)
      unique?     (.unique)
      index-only  (.indexOnly (vertex-label mgmt index-only)))))

(defn build-composite-index!
  "Create a composite index."
  [^TitanManagement mgmt ^String name ^Class el-type & {:as options}]
  (.buildCompositeIndex (index-builder mgmt name el-type options)))

(defn build-mixed-index!
  "Creates a mixed key"
  [^TitanManagement mgmt ^String name ^Class el-type & {:as options}]
  {:pre (contains? options :backing-index)}
  (.buildMixedIndex (index-builder mgmt name el-type options)
                    (:backing-index options)))

(defn build-edge-index!
  "Creates an edge index"
  [^TitanManagement mgmt ^String name label
   & {:keys [direction order keys] :or {direction :both order :asc}}]
  (.buildEdgeIndex mgmt (edge-label mgmt label) name
                   (to-direction direction)
                   (to-order order)
                   (into-array
                    (map #(relation-type mgmt %) keys))))

(defn build-property-index!
  "Create a property key index"
  [^TitanManagement mgmt ^String ^PropertyKey property-key name
   & {:keys [direction order key] :or {direction :both :order :asc}}]
  (.buildEdgeIndex mgmt name
                   (to-direction direction)
                   (to-order order)
                   (into-array
                    (cond
                      (nil? key) []
                      (coll? key) key
                      :else [key]))))

(defn- to-consistency [c]
  (case c
    :default ConsistencyModifier/DEFAULT
    :fork    ConsistencyModifier/FORK
    :lock    ConsistencyModifier/LOCK
    (throw (ex-info "invalid consistency modifier" {:consistency c}))))

(defn set-consistency!
  [^TitanManagement mgmt ^TitanSchemaElement el consistency]
  (.setConsistency mgmt el (to-consistency consistency)))


(defn ^TitanGraph create-graph!
  [props]
  (let [^TitanFactory$Builder config (TitanFactory/build)]
    (doseq [[^String k ^String v] props]
      (.set config k v))
    (.open config)))

(defn titan-env-fixture
  [props callback]
  (fn [f]
    (let [^TitanGraph g (create-graph! props)]
      (callback g)
      (.shutdown g))))
